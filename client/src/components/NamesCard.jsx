import "../styles/namecard.css";
import React, { useState } from "react";

const DoctorCard = ({ name,language,bio }) => {


    return (
        <div className={`card`}>
            <h3 className="specialization">
            <strong>Name: </strong>
                {name}
            </h3>
            <p className="specialization">
            <strong>Language: </strong>
                {language}
            </p>
            <p className="experience">
                <strong>Bio: </strong>
                {bio}
            </p>
        </div>
    );
};

export default DoctorCard;
