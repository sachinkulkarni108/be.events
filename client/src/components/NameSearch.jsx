import axios from "axios";
import React, { useState, useEffect } from "react";
import "../styles/name.css";
import Loading from "../components/Loading";
import NamesCard from "../components/NamesCard"

const NameSearch = () => {
    const [data, setData] = useState([]);
    const [filteredResults, setFilteredResults] = useState([]);
    const [Name, setName] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            setError(null);

            try {
                const response = await axios.get("http://localhost:5000/api/getNames", {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem("token")}`,
                    },
                });
                console.log('response',response)
                setData(response.data);
            } catch (err) {
                console.error("Error fetching data:", err);
                setError(err);
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();
    }, []); // Run useEffect only once on component mount

    const handleSearch = (event) => {
        event.preventDefault();

        if (!Name) {
            alert("Please enter a name to search.");
            return;
        }

        const filtered = data?.filter((person) =>
            person?.name.toLowerCase().includes(Name.toLowerCase())
        );
        setFilteredResults(filtered);
    };
    console.log(filteredResults, "results")
    return (
        <section className="apply-name-section flex-center">
            {isLoading && <Loading />}
            {!isLoading && (
                <div className="apply-name-container flex-center">
                    <div className="form-heading">
                        <h2 className="form-heading">Enter Name</h2>
                        <form className="register-form" onSubmit={handleSearch}>

                            <input
                                type="text"
                                className="form-input"
                                name="name"
                                value={Name}
                                onChange={(event) => setName(event.target.value)}
                                required
                            />
                            <button className="btn form-btn" type="submit">Search</button>
                        </form>
                        {isLoading && <div>Loading data...</div>}
                        {error && <div>Error: {error.message}</div>}
                        {filteredResults.length > 0 && (
                            <div id="search-results">
                                <section className="container doctors">
                                    <div className=".name-card-container">
                                        <ul>
                                            {filteredResults.map((person) => (

                                                <NamesCard
                                                    name={person.name}
                                                    language={person.language}
                                                    bio={person.bio}
                                                />
                                            ))}
                                        </ul>
                                    </div>
                                </section>

                            </div>
                        )}
                    </div>
                </div>
            )}
        </section>
    );
};

export default NameSearch;
