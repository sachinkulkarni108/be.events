import React from "react";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import NameSearch from "../components/NameSearch"
const SearchName = () => {
  return (
    <>
      <Navbar />
      <NameSearch/>     
      <Footer />
    </>
  );
};
export default SearchName