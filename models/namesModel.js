const mongoose = require("mongoose");

const schema = mongoose.Schema(
    {
        name: {
            type: String
        },
        language: {
            type: String
        },
        id: {
            type: String
        },
        bio: {
            type: String
        },
        version: {
            type: Number
        },

    },
    {
        timestamps: true,
    }
);

const namesModel = mongoose.model("namesModel", schema);

module.exports = namesModel;