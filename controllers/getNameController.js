const namesModel = require("../models/namesModel");


const getNames = async (req, res) => {
  console.log("hi")
  try {
    const names= await namesModel.find()
    console.log(names,'names')
    res.json(names);
  } catch (error) {
    console.log('names error')
    res.status(500).send("Unable to get names");
  }
};

module.exports = {getNames}