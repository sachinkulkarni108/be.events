const express = require("express");
const names = require("../names.json");
const namesModel = require("../models/namesModel");


async function LanguageController() {
    const insertResult = await namesModel.insertMany(names);
    console.log('Inserted documents:', insertResult.length);
    return 'Done inserting documents.';
}

module.exports = LanguageController