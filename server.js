const express = require("express");
const cors = require("cors");
require("dotenv").config();
require("./db/conn");
const userRouter = require("./routes/userRoutes");
const path = require("path");
const language = require("./controllers/languageController");
const getName=require("./routes/getNamesRoutes")
const app = express();
const port = process.env.PORT || 5000;



language()

app.use(cors());
app.use(express.json());
app.use("/api/user", userRouter);
app.use("/api",getName)

app.listen(port, () => {console.log(`it is working on port ${port}`)});
