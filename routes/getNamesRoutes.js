const express = require("express");
const auth = require("../middleware/auth");
const getNames = require("../controllers/getNameController");

const nameRouter = express.Router();
nameRouter.get(
  "/getNames",
  auth,
  getNames.getNames
);

module.exports =nameRouter
